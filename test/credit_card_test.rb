require 'minitest/autorun'
require_relative '../lib/credit_card'


class CreditCardTest < Minitest::Test

  def setup
    @card = CreditCard.new("Mary", 5454545454545454, "$1000")
    @invalid_card = CreditCard.new("Mary", 54545454545454541111111111, "$1000")
  end

  def test_valid_credit_card_data
    assert_equal(@card.limit, 1000) 
    assert_equal(@card.balance, "$0")
    assert_equal(@card.name, "Mary")
    assert_equal(@card.number, 5454545454545454)     	
  end

  def test_invalid_card_number
    assert @invalid_card.number_valid? != true 
  end

  def test_if_number_valid
    assert @card.number_valid?
  end

  def test_limit_format
    skip
  end


end