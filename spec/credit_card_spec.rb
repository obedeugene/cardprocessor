require 'rspec'
require 'credit_card'

describe 'CreditCard' do
  
  context 'when card number is valid' do 
    let(:card) {CreditCard.new("Quincy", 5454545454545454, "$1000")}
    
    it 'gets created' do
      expect((card).number_valid?).to be true
    end

    it 'gets created with a 0 balance' do
      expect((card).balance).to eq("$0")
    end
  end

  context 'when card number is not valid' do 
    let(:card) {CreditCard.new("Quincy", 41112222333344445555, "$1000")}
    
    it 'does not get created' do
      expect((card).number_valid?).to be false
    end
  end
end


  # context 'when generating card Summary' do 
  #   before(:each) do
  #     processor.add  

