require 'rspec'
require 'credit_card'


describe 'Processor' do
  let(:processor) { Processor.new }
 
  context 'When Adding a new CreditCard' do
    it 'creates a new card with name, number, limit' do
      processor.add("Tom", 5454545454545454, "$1000")
      johndoe = processor.accounts["Tom"]
      
      expect(johndoe.name).to eq("Tom")
      expect(johndoe.number).to eq(5454545454545454)
      expect(johndoe.limit).to eq(1000)
    end
  end

  context 'when a Charge is made on CreditCard' do
	before(:each) do
	  processor.add("Tom", 5454545454545454, "$1000")
	  processor.charge("Tom", "$200")
	end 

  	it 'increases the CreditCard balance' do 
      expect(processor.accounts["Tom"].balance).to eq("$200")    
    end

    it 'does not change CreditCard balance when charge exceeds limit' do 
      processor.charge("Tom", "$2000")      
      expect(processor.accounts["Tom"].balance).to eq("$200")
    end  
  end

  context 'when Credit is added to CreditCard account' do 
  	before(:each) do 
  	  processor.add("Tom", 5454545454545454, "$1000")
  	  processor.charge("Tom", "$200")
  	end

  	it 'decreases the CreditCard balance' do
  	  processor.credit("Tom", "$100")
  	  expect(processor.accounts["Tom"].balance).to eq("$100")
  	end

  	it 'creates a negative balance when credit is greater than balance' do
      processor.credit("Tom", "$500")
      expect(processor.accounts["Tom"].balance).to eq("$-300")
    end
  end

  # context 'when generating card Summary' do 
  #   before(:each) do
  #     processor.add  
end
