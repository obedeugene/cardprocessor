# CardProcessor

###Introduction
CardProcessor is a program that processes CreditCard transactions. The program validates if the card by using the Luhn10 algorithm and other standards. This was designed using TDD with Rspec.

### Getting started
Install required gems:

gem install luhn-ruby  
gem install rspec  
gem install minitest

### How to use
To run program, place all data in the input.txt file. Program supports Data in this format:

Add Tom 4111111111111111 $1000
Charge Tom $500
Credit Lisa $100

Then run program in terminal by typing command:

ruby run.rb


### Design
I designed the program to be extendable to support any additional payment options. The CreditCard class handles all methods that is associated with the CreditCard, and the Processor Class handles all methods that is associated with processing any card option. 

