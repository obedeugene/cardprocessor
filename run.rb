require_relative 'lib/credit_card.rb'

processor = Processor.new

input = File.read("input.txt")

input.split("\n").each do |line|
  processor.input(line)
end

processor.summary