require 'luhn'
require_relative '../lib/credit_card'

class Processor 
  attr_accessor :accounts
  
  def initialize
    @accounts = {}
  end
  
  def add(name, number, limit)
    card = CreditCard.new(name, number, limit)
  	@accounts[name] = card
  end

  def charge(name, amount)
    accounts[name].charge(amount)
  end

  def credit(name, amount)
  	accounts[name].credit(amount)  
  end 

  def summary
    accounts.keys.sort.each { |name| puts accounts[name] }
  end

  def input(string)
    input = string.split(' ')
    method = input.shift.downcase.to_sym
    self.send(method, *input)
  end  
end



