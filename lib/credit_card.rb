require 'luhn'
require_relative '../lib/processor'

class CreditCard
  attr_reader :name, :limit, :balance, :number

  def initialize(name, number, limit)
	@name    = name
	@number  = number
	@limit   = limit 
	@balance = 0
  end 

  def number_valid?
     @number.to_s.length < 20
     Luhn.valid? @number.to_s
  end

  def limit
    (@limit[/\$\d+/,0].delete "$").to_i 
  end

  def value(amount) 
    (amount[/\$\d+/,0].delete "$").to_i
  end 

  def charge(amount)
  	if number_valid?
  	  amount = value(amount)
      @balance += amount unless @balance + amount > limit
    end
  end
  
  def credit(amount)
    if number_valid? 
      amount = value(amount) 
      @balance -= amount
    end
  end

  def balance
    number_valid? ? "$#{@balance}" : 'error'
  end

  def to_s
    "#{name}: #{balance}"
  end
end   
